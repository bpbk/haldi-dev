<?php get_header(); ?>

	<main role="main">

		<section>

		<?php if (have_posts()): while (have_posts()) : the_post();
			$user_id = $post->post_author;
			$user_type = get_field('user_type', 'user_' . $user_id);
			$myUserData = get_userdata($user_id);
			$pageID = get_the_ID();
			$hasRecs = false;
			$full_profile = false;
			if ( $collections_url = get_field('collections_url', 'user_' . $user_id) ) {
				if ( stripos($collections_url, 'profile.haldiskin') != false ) {
					$full_profile =true;
				}
			}
			wp_localize_script( 'html5blankscripts', 'currentUserEmail', $myUserData->user_email );
			wp_localize_script( 'html5blankscripts', 'currentRoutineID', (string)$pageID );
			$referrals = []; // Referrals only
			$allReferrals = []; // Referrals plus referrer
			if( $refs = get_field('referrals', 'user_' . $user_id) ) {
				foreach($refs as $ref) {
					array_push($referrals, $ref);
					array_push($allReferrals, $ref);
				}
			}
			if( $ref = get_field('referrer', 'user_' . $user_id) ) {
				array_push($allReferrals, $ref);
			} ?>
			<div class="side-nav fixed">
				<?php
				$sections = [];
				if ( get_the_content() && $full_profile ) {
					array_push($sections, [
						'class' => 'profile',
						'anchor' => 'profile',
						'hover' => 'Profile'
					]);
				}
				if ( have_rows('recommended_products') && $full_profile) {
					array_push($sections, [
						'class' => 'products',
						'anchor' => 'recommended-products',
						'hover' => 'Recommended Products'
					]);
				}
				if ( have_rows('routine') && $full_profile ) {
					array_push($sections, [
						'class' => 'routine',
						'anchor' => 'recommended-routine',
						'hover' => 'Recommended Routine'
					]);
				}
				if( count($referrals) > 0 || get_field('user_has_purchased', 'user_' . $user_id) && $full_profile ) {
					array_push($sections, [
						'class' => 'referrals',
						'anchor' => 'referrals',
						'hover' => 'Referrals'
					]);
				}
				if(count($sections) > 1) {
					foreach ($sections as $key=>$section) { ?>
						<a data-hover="<?php echo $section['hover']; ?>" class="number <?php echo $section['class']; ?> bg-check" href="#<?php echo $section['anchor']; ?>">
							<?php echo $key + 1; ?>
						</a>
					<?php
					}
				} ?>
			</div>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<!-- post title -->
				<?php if( have_rows('recommended_products') && $full_profile ):
					$hasRecs = true; ?>
					<div class="content-inner">
						<h1 class="hide">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						</h1>
						<div class="grid post-body">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="wrapper wrapper-recommend scroll-section" id="recommended-products">
						<div class="recommended-products content-inner">

							<h2>Your Recommended Products</h2>
	            <?php while ( have_rows('recommended_products') ) : the_row(); ?>
	              <?php if( $productID = get_sub_field('product') ): ?>
									<div class="product-wrapper grid">
										<div class="product-meta">
											<div class="product-function">
												<?php echo get_field('product_type', $productID); ?>
											</div>
											<div class="product-solves-for">
												<div class="title">
													Solves for:
												</div>
												<?php
													if( get_sub_field('solves_for_overwrite') ) {
														echo "<span>";
														echo get_sub_field('solves_for_overwrite');
														echo "</span>";
													}
													else {
														echo "<span>";
														echo get_field('solves_for', $productID);
														echo "</span>";
													}
												?>
											</div>
										</div>
										<div class="product-meta-line"></div>
										<div class="product-image">
											<img src="<?php echo get_the_post_thumbnail_url($productID, 'large'); ?>"/>
											<?php
											// If they have referrals, find referrals with similar products
											if( count($allReferrals) > 0 ) {
												// Get referrals
												$args = array(
													'numberposts'	=> -1,
													'post_type'		=> 'post',
													'post_status' => 'publish',
													'author__in' => $allReferrals,
													'meta_query'	=> array(
														array(
															'key'		=> 'recommended_products_$_product',
															'compare'	=> '=',
															'value'		=> $productID,
														)
													)
												);
												$the_query = new WP_Query( $args );

												?>
												<?php if( $the_query->have_posts() ): ?>
													<div class="shared-products">
														<p class="shared-products-label">Also in your friend's routine</p>
														<p class="shared-products-sub-label">Click to view</p>
														<div class="referrals-shared-products">
															<?php
															while ( $the_query->have_posts() ) : $the_query->the_post();
																$_userID = get_the_author_meta( 'ID' );
																$refData = get_userdata($_userID);
																displayRef($refData);
															endwhile; wp_reset_query(); ?>
														</div>
													</div>
												<?php endif;
											} ?>
										</div>
										<!-- <hr class="product-image-hr"> -->
										<div class="product-content">
											<span class="product-label"><?php echo get_field("label", $productID);?></span>
											<h4><?php echo get_the_title($productID); ?></h4>
											<?php
											$product = get_post($productID);
											$content = $product->post_content;
											$content = apply_filters('the_content', $content);
											$content = str_replace(']]>', ']]&gt;', $content);
											echo $content;
											if( get_sub_field('coachs_note') ) {
											?>
												<div class="coach-note">
													<h3>Notes</h3>
														<?php echo get_sub_field('coachs_note'); ?>
												</div>
												<?php
											}

											?>
											<span class="cost-amount">
												<h5>$<?php echo get_field('cost', $productID); ?></h5>
												<h6><?php echo get_field('amount', $productID); ?></h6>
											</span>
										</div>
									</div>
	              <?php endif; ?>
	          	<?php endwhile; ?>
							<?php wp_reset_query(); ?>
						</div>
					</div>
		    <?php endif; ?>

				<?php if( have_rows('routine') && $full_profile ): ?>
					<div class="outer-wrapper outer-wrapper-routine">
						<div class="wrapper wrapper-routine scroll-section" id="recommended-routine">
							<div class="recommended-routine content-inner">
								<h2>Your Personal Routine</h2>

								<div class="routine-toggle">
									<!--<?php while ( have_rows('routine') ) : the_row(); ?>
										<span class="routine-label-toggle" id="<?php echo get_sub_field('routine_label'); ?>">
											<h3 class="routine-label">
												<?php echo get_sub_field('routine_label'); ?>
											</h3>
										</span>
									<?php endwhile; ?>-->
									<div class="toggle-wrapper">
										<div class="toggle-inner-wrapper">
											<span>Morning</span>
											<input type="checkbox" class="jtoggler">
											<span>Night</span>
										</div>
									</div>
								</div>

								<div class="routines-wrapper">
									<?php
									while ( have_rows('routine') ) : the_row();
										if( get_sub_field('products') ):
											$count = 1; ?>
											<div class='routine-wrapper routine-<?php echo get_sub_field('routine_label'); ?>'>
												<h3 class="routine-label">
													<?php echo get_sub_field('routine_label'); ?>
												</h3>
												<div class='routine-inner-wrapper'>
													<?php
													while ( have_rows('products') ) : the_row();
														if( get_sub_field('client_product') ): ?>
															<div class="product">
																<?php $image = get_sub_field('alt_image');
																	if( !empty($image) ): ?>
																		<img src="<?php echo $image['url']; ?>" class="product-image" />
																<?php endif; ?>
																<div class="number">
																	<?php echo $count; ?>
																	<div class="title">
																		<?php echo get_sub_field('client_product');	?>
																	</div>
																</div>
																<div class="title outside">
																	<?php echo get_sub_field('client_product');	?>
																</div>
															</div>

														<?php else: ?>
															<div class="product">
																<?php if( $productID = get_sub_field('haldi_product') ): ?>
																	<img src="<?php echo get_the_post_thumbnail_url($productID); ?>" class="product-image" />
																<?php endif; ?>
																<div class="number">
																	<?php echo $count; ?>
																	<div class="title">
																		<?php echo get_field("label", $productID);?> <?php echo get_the_title($productID); ?>
																	</div>
																</div>
																<div class="title outside">
																	<?php echo get_field("label", $productID);?> <?php echo get_the_title($productID); ?>
																</div>
															</div>
														<?php endif;
														$count++;
													endwhile;
													wp_reset_query(); ?>
												</div>
											</div>
										<?php
										endif;
									wp_reset_query();
									endwhile; ?>
								</div>
								<a class="button" href="<?php echo get_field('collection_url'); ?>" target="_blank">
									<span>shop your routine</span>
								</a>
							</div>
						</div>
						<?php if( get_field('user_has_purchased', 'user_' . $user_id) ) { ?>
							<div class="content-inner">
								<hr>
							</div>
						<?php } ?>
					</div>
				<?php endif; ?>
				<?php
				if( count($referrals) > 0 || get_field('user_has_purchased', 'user_' . $user_id) ) { ?>
					<div class="outer-wrapper">
						<div class="wrapper wrapper-referrals scroll-section <?php echo $full_profile ? 'has-recs' : 'no-recs'; ?>" id="referrals">
							<div class="content-inner">
								<?php
								$myInfo = get_userdata($user_id);

								function active_ref($user) {
									if ( get_field('user_has_purchased', 'user_' . $user) ) {
										return $user;
									}
								}
								$profile_title = $myInfo->first_name;
								$profile_title .= substr($myInfo->first_name, -1) === 's' ? '\'' : '\'s';
								$activeReferrals = array_values(array_filter($referrals, 'active_ref')); ?>

								<h2><?php echo $profile_title; ?> Haldi friends</h2>
								<div class="referrals-header">
									<div class="haldi-credit">
										<?php
										// $tens = floor(count($activeReferrals) / 10);
										// $total_credit = ( 500 * $tens ) + ( (count($activeReferrals) % 10) * 20);
										$total_credit = 0;
										$redeemed_credit = get_field('field_5c90039e56de4', 'user_' . $user_id);
										if( count($activeReferrals) < 10 ) {
											$total_credit = count($activeReferrals) * 20;
										} else {
											$total_credit = 500 + ((count($activeReferrals) - 10) * 20);
										} ?>
										<p>Haldi credit to date: <?php echo '$' . $total_credit; ?></p>
										<?php
										if ( $total_credit > 0 ) { ?>
											</br>
											<sub>Current balance: <?php echo '$' . ($total_credit - $redeemed_credit); ?></sub>
										<?php
										} ?>
									</div>
								</div>
								<?php

								// Get Referrer
								if ( $referrer = get_field('referrer', 'user_' . $user_id) ) {
									$referrerData = get_userdata($referrer); ?>
									<!-- <div class="referrer">
										You were referred by: <?php echo $referrerData->user_nicename; ?>
									</div> -->
								<?php
								} ?>
								<div class="referrals-wrapper">
									<div class="referrals<?php echo count($activeReferrals) > 10 ? ' hide-meter' : ''; ?>">
										<?php
										// Inative Referrals
										function inactive_ref($user) {
											if ( get_field('user_has_purchased', 'user_' . $user) == false ) {
												return $user;
											}
										}
										$inactiveReferrals = array_values(array_filter($referrals, 'inactive_ref')); ?>

										<div class="referrals-container inactive-referrals">
											<?php
											if( count($inactiveReferrals) > 0 ) { // Has active referrals ?>
												<div class="referral-description">
													<div>Nudge your friends along!</div>
													<div class="referral-arrow">
														<img src="<?php echo get_template_directory_uri(); ?>/img/arrow1.svg"/>
													</div>
												</div>
												<h5>Friends who have filled out the survey</h5>
												<div class="referrals-list-container">
													<div class="referrals-list">
														<?php displayReferrals($inactiveReferrals); ?>
													</div>
												</div>
											<?php
											} ?>
										</div>

										<?php // Active Referrals	?>

										<div class="referrals-container active-referrals">
											<div class="referral-description">
												<div>Thanks for spreading the word!</div>
												<div class="referral-arrow">
													<img src="<?php echo get_template_directory_uri(); ?>/img/arrow2.svg"/>
												</div>
											</div>
											<div class="referral-meter-container">
												<?php
												if( count($activeReferrals) > 0 ) { ?>
													<h5>Friends who have purchased their routine</h5>
												<?php
												}
												if( count($activeReferrals) == 0 && count($inactiveReferrals) == 0) { ?>
													<h5>Tell your friends about Haldi and earn free skincare</h5>
												<?php
												}
												if( count($activeReferrals) == 0 && count($inactiveReferrals) > 0) { ?>
													<h5>Friends who have purchased their routine</h5>
												<?php
												} ?>
												<div class="referral-meter">
													<?php
													for ( $r = 0; $r <= 9; $r++ ) { ?>
														<div class="referral-placeholder-container">
															<div class="referral-placeholder">
																<?php
																if(isset($activeReferrals[$r])) {
																	$refData = get_userdata($activeReferrals[$r]);
																	displayRef($refData);
																}
																switch($r) {
																	case 0: ?>
																		<span><?php echo $r + 1; ?></span>
																		<div class="meter-marker">
																			<p>$20</p>
																			<span>1 friend</span>
																		</div>
																	<?php
																	break;
																	case 4: ?>
																		<span><?php echo $r + 1; ?></span>
																		<div class="meter-marker marker-100">
																			<p>$100</p>
																			<span>5 friends</span>
																		</div>
																	<?php
																	break;
																	case 9: ?>
																		<span><?php echo $r + 1; ?></span>
																		<div class="meter-marker">
																			<p>$500</p>
																			<span>10 friends</span>
																		</div>
																	<?php
																	break;
																	default:
																	break;
																} ?>
															</div>
														</div>
													<?php
													}
													if ( count($activeReferrals) > 10 ) {
														$activeAfterTen = array_slice($activeReferrals, 10);
														foreach($activeAfterTen as $ref) { ?>
															<div class="referral-placeholder-container">
																<div class="referral-placeholder">
																	<?php
																	$refData = get_userdata($ref);
																	displayRef($refData); ?>
																</div>
															</div>
														<?php
														}
													} ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php
								$referralsLeft = false;
								if( count($activeReferrals) < 10 && count($activeReferrals) > 0) {
									$referralsLeft = 'Refer ' . (10 - count($activeReferrals)) . ' more friends to earn $500 towards skincare';
								} else if ( count($activeReferrals) == 10) {
									$referralsLeft = false;
								}
								if ($referralsLeft != false) { ?>
									<p class="mobile-credit-desc"><?php echo $referralsLeft; ?></p>
								<?php
								}
								?>
								<div class="referral-instructions">
									<?php if ( $discount_code = get_field('shopify_discount_code', 'user_' . $user_id ) ) { ?>
										<div class="discount-code">
											<p>To redeem your Haldi credit, use <span><?php echo $discount_code; ?></span> at checkout</p>
										</div>
									<?php
									} ?>
									<p>Thanks for spreading the word!</br>Learn more about the <a href="https://www.haldiskin.com/refer-a-friend" target="_blank">Haldi Friends Program</a>.</p>
									<div class="sub-instructions">
										<sub>
											Your Haldi Friends tracker is updated weekly.
										</sub>
									</div>
								</div>

							</div>
						</div>
					</div>
				<?php
				}
				if( $full_profile ) {
					$collectionsURL = get_field('collection_url'); ?>
					<div class="outer-wrapper to-cross">
						<div class="wrapper wrapper-next dark-bg scroll-section" id="recommended-routine">
							<div class="content-inner">
									<div class="grid post-body">
										<?php include('next.php'); ?>
										<a class="button" href="<?php echo $collectionsURL; ?>" target="_blank">
											<span>shop your routine</span>
										</a>
									</div>
							</div>
						</div>
					</div>
				<?php } ?>

				<!--
					<div class="content-inner">
						<div class="grid post-body">
							<div class="slogan">
									Free shipping. Free returns.
							</div>
						</div>
					</div>
				-->
				<?php
				if(count($allReferrals) > 0) { ?>
					<div class="referral-popup">
						<div class="referral-popup-wrapper">
							<div class="referral-popup-container">
								<div class="referral-popup-close">

								</div>
								<div class="referral-popup-content">

								</div>
							</div>
						</div>
					</div>
				<?php
				} ?>
			</article>
		<?php
	endwhile; ?>
		<?php wp_reset_query(); ?>
		<?php else: ?>

			<article>
				<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
			</article>
			<!-- /article -->
		<?php endif; ?>

		</section>
	</main>



<?php get_footer(); ?>
