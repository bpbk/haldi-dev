<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="first">
			<div class="wrapper">

				<h2><?php the_title(); ?></h2>

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php the_content(); ?>

				</article>
				<!-- /article -->

			<?php endwhile; ?>

			<?php else: ?>

			<?php endif; ?>

			</div>
		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
