
			<footer class="footer" role="contentinfo">
				<div class="content-inner">
					<div class="footer-content">
						<p>
							<img class="footer-logo" src="<?php bloginfo('template_url'); ?>/img/logo.png"/>
						</p>

						<div class="social-links">
							<a href="https://www.instagram.com/haldiskin/?hl=en" target="_blank" alt="instagram" class="instagram">
								<?php get_template_part( 'img/icons/instagram.svg' ); ?>
							</a>
							<a href="https://www.facebook.com/haldiskin" target="_blank" alt="facebook" class="facebook">
								<?php get_template_part( 'img/icons/facebook.svg' ); ?>
							</a>
						</div>
					</div>
				</div>
			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<script type="text/javascript">
			$(document).ready(function () {
				$(function(){
				  $('.jtoggler').jtoggler();
				});
			});

		$(document).ready(function () {
				var $window = $(window),
						$slick,
						toggleSlick;

				if ( $('.slider').length ) {
					$slick = $('.slider');
					toggleSlick = function () {
						if ($window.width() > 600) {
							$slick.slick("unslick");
						} else {
							$slick.slick({
								// normal options...
								infinite: false,
								slidesToShow: 1,
								dots: true,
								prevArrow: false,
								nextArrow: false,
							});
						}
					}
					$window.resize(toggleSlick);
					toggleSlick();

				}

		});
		$(document).on('jt:toggled', function(event, target) {
			$wrapper = $(".wrapper-routine");
			$outerWrapper = $(".outer-wrapper-routine");
			$fixed = $(".fixed");
			if ($('input.jtoggler').is(':checked')) {
				$(".routine-wrapper").addClass("inactive").removeClass("active");
				$(".routine-wrapper:nth-of-type(2)").addClass("active").removeClass("inactive");
				$(".routine-wrapper:nth-of-type(3)").addClass("active").removeClass("inactive");
				$wrapper.addClass("dark")
				$outerWrapper.addClass("to-cross");
				$fixed.addClass('light');
			}
			else {
				$(".routine-wrapper").addClass("inactive").removeClass("active");
				$(".routine-wrapper:nth-of-type(1)").addClass("active").removeClass("inactive");
				$wrapper.removeClass("dark")
				$outerWrapper.removeClass("to-cross");
				$fixed.removeClass('light');
			}
			});

	</script>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134776213-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-134776213-1');
	</script>


	</body>
</html>
