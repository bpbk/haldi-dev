$(document).ready(function() {
	$('.side-nav a').on('click', function(e) {
		e.preventDefault(); // prevent hard jump, the default behavior
			var target = $(this).attr("href"); // Set the target as variable
			var targetScroll = $(target).offset().top;
			console.log(targetScroll)
			// perform animated scrolling by getting top-position of target-element and set it as scroll target
			$('html, body').animate({
					scrollTop: targetScroll
			}, 600, function() {
					history.replaceState(null, null, document.location.pathname + target); //attach the hash (#jumptarget) to the pageurl
					return false;
			});
	});

});


$(window).scroll(function() {
		const scrollDistance = $(window).scrollTop();
		const headerHeight = $('header').height();
		const windowHeight = $(window).height();
		// Show/hide menu on scroll
		//if (scrollDistance >= 850) {
		//		$('nav').fadeIn("fast");
		//} else {
		//		$('nav').fadeOut("fast");
		//}

		// Assign active class to nav links while scolling
		$('.scroll-section').each(function(i) {
			const thisTop = $(this).position().top
			const thisHeight = $(this).height()
			// console.log('pos:'+$(this).position().top, 'scroll:'+scrollDistance);
				if ( thisTop - headerHeight - ( windowHeight / 2 ) <= scrollDistance && thisTop - headerHeight + ( windowHeight / 2 ) + thisHeight > scrollDistance) {
						$('.side-nav a.active').removeClass('active');
						$('.side-nav a').eq(i).addClass('active');
				}
		});
})
