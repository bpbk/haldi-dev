<?php
class CSVExport {

  /**
   * Constructor
   */
  public function __construct() {
    if (isset($_GET['haldi_report'])) {
      $pdf_name = $_GET['haldi_report'];
      if(isset($_GET['haldi_report_date'])) {
        $report_date = $_GET['haldi_report_date'];
        $after_date = date( 'm_d_Y', $report_date );
        $pdf_name .= '_' . $after_date;
      }
      $csv = $this->generate_csv();
      header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Cache-Control: private", false);
      header("Content-Type: application/octet-stream");
      header("Content-Disposition: attachment; filename=\"haldi_" . $pdf_name . ".csv\";");
      header("Content-Transfer-Encoding: binary");
      echo $csv;
      exit;
    }

    if ( isset( $_GET['haldi_user_json'] ) ) {
      $user_json = $this->generate_user_json();
      header('Content-disposition: attachment; filename=haldi_user_data.json');
      header('Content-type: application/json');
      echo $user_json;
      exit;
    }

// Add extra menu items for admins
    add_action('admin_menu', array($this, 'admin_menu'));

// Create end-points
    add_filter('query_vars', array($this, 'query_vars'));
    add_action('parse_request', array($this, 'parse_request'));
    add_action('parse_json_request', array($this, 'parse_json_request'));
  }

  public function generate_user_json () {
    $users_obj = [];
    $args = array(
    	'role'         => 'subscriber',
    	'orderby'      => 'meta_value',
      'meta_key'     => 'last_name',
    	'order'        => 'ASC',
    	'fields'       => 'all',
     );
    $subscribers = get_users( $args );
    foreach( $subscribers as $sub ) {
      $user_obj = [];
      $user_id = $sub->ID;
      $user_data = get_userdata( $user_id );
      $email = $user_data->user_email;
      if ( $routine_id = get_field('field_5c65cddfa2852', 'user_' . $user_id) ) {
        if ( $routine = get_post($routine_id) ) {
          $user_obj['coachs_note'] = apply_filters( 'the_content', $routine->post_content );
          $users_obj[$email] = $user_obj;
        }
      }
    }
    return json_encode($users_obj);
  }

  /**
   * Add extra menu items for admins
   */
  public function admin_menu() {
    add_menu_page('Export Referrals', 'Export Referrals', 'manage_options', 'export_referrals', array($this, 'export_referrals'));
    add_menu_page('Export User JSON', 'Export User JSON', 'manage_options', 'export_user_json', array($this, 'export_user_json'));
  }

  /**
   * Allow for custom query variables
   */
  public function query_vars($query_vars) {
    $query_vars[] = 'export_referrals';
    $query_vars[] = 'export_user_json';
    return $query_vars;
  }

  /**
   * Parse the request
   */
  public function parse_request(&$wp) {
    if (array_key_exists('export_referrals', $wp->query_vars)) {
      $this->export_referrals();
      exit;
    }
  }

  public function parse_json_request(&$wp) {
    if (array_key_exists('export_user_json', $wp->query_vars)) {
      $this->export_user_json();
      exit;
    }
  }

  /**
   * Download reports
   */
  public function export_referrals() {
    ob_start(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <div class="wrap">
      <div id="icon-tools" class="icon32">
      </div>
      <h2>Export Referral Data</h2>
      <p><a href="?page=export_referrals&haldi_report=totals">Export Totals</a></p>
      <!-- <p><a href="?page=export_referrals&haldi_report=weekly_report">Export Weekly Report</a></p> -->
      <form>
        <div class='input-group date' id='datetimepicker1'>
           <input type='text' class="form-control" />
           <span class="input-group-addon">
           <span class="glyphicon glyphicon-calendar"></span>
           </span>
        </div>
        <input type="hidden" id="haldi_report_date" name="haldi_report_date" value="weekly_report">
        <input type="hidden" id="haldi_report" name="haldi_report" value="weekly_report">
        <input type="submit" class="btn btn-primary" value="Export Weekly Report">
      </form>

      <h2>Export User Data for Migration</h2>
      <p><a href="?page=export_user_json&haldi_user_json=totals">Export Totals</a></p>
    </div>
    <script>
      $(function () {
        var lastWeek = new Date();
            lastWeek = lastWeek.setDate(lastWeek.getDate() - 7);

        $('#datetimepicker1').datetimepicker({
          format: 'MM/DD/YYYY',
          defaultDate: lastWeek
        });
        var lastWeekUnix = $('#datetimepicker1').data('DateTimePicker').date().unix()
        $('#haldi_report_date').val(lastWeekUnix);
        console.log(lastWeekUnix)

        $("#datetimepicker1").on("dp.change", function (e) {
          var unixDate = $(this).data('DateTimePicker').date().unix()
          console.log(unixDate)
          $('#haldi_report_date').val(unixDate);
        });
     });
    </script>
    <?php
    echo ob_get_clean();
  }

  public function export_user_json() {
    ob_start(); ?>
    <div class="wrap">
      <div id="icon-tools" class="icon32">
      </div>
      <h2>Export User Data for Migration</h2>
      <p><a href="?page=export_user_json&haldi_user_json=users">Export User JSON</a></p>
    </div>
    <?php
    echo ob_get_clean();
  }

  /**
   * Converting data to CSV
   */
  public function generate_csv () {
    $csv_output = '';

    switch( $_GET['haldi_report'] ) {
      case 'totals' :
        $csv_output = $this->get_referral_totals();
      break;
      case 'weekly_report' :
        $csv_output = $this->get_weekly_report();
      break;
    }


    return $csv_output;
  }

  public function get_referral_totals () {
    $csv_output = '';
    $args = array(
    	'role'         => 'subscriber',
    	'orderby'      => 'meta_value',
      'meta_key'     => 'last_name',
    	'order'        => 'ASC',
    	'fields'       => 'all',
     );
    $subscribers = get_users( $args );
    $csv_output .= 'date added, first name, last name, haldi credit';
    $csv_output .= "\n";

    foreach( $subscribers as $sub ) {
      $sub_data = get_userdata( $sub->ID );
      $csv_output .= $sub_data->user_registered . ',';
      $csv_output .= get_user_meta( $sub->ID, 'first_name', true ) . ',';
      $csv_output .= get_user_meta( $sub->ID, 'last_name', true ) . ',';

      // Find referral credit
      $total_credit = 0;
      if ( $referrals = get_field( 'field_5c65abc5f6ab5', 'user_' . $sub->ID )) {
        $active_refs = [];
        foreach($referrals as $ref) {
          // If user has purchased
          if ( get_field( 'field_5c65c66748656', 'user_' . $ref ) ) {
            array_push( $active_refs, $ref);
          }
        }
        if( count($active_refs) < 10 ) {
          $total_credit = count($active_refs) * 20;
        } else {
          $total_credit = 500 + ((count($active_refs) - 10) * 20);
        }
      }
      $csv_output .= '$'.$total_credit;
      $csv_output .= "\n";
    }
    return $csv_output;
  }

  public function get_weekly_report () {
    // Get all subscribers
    //$after_date = date('Y-m-d', strtotime('last Saturday', strtotime('today')));
    $report_date = $_GET['haldi_report_date'];
    $after_date = gmdate( 'Y-m-d', $report_date );
    $purchased_args = [
      'meta_key' => 'first_purchase_date',
      'meta_query'  => [
        'relation' => 'AND',
        [
          'key'     => 'first_purchase_date',
          'value'   => $after_date,
          'compare' => '>',
          'type'    => 'DATE'
        ],
        [
          'key'   => 'user_has_purchased',
          'compare' => 'EXISTS',
        ]
      ]
    ];

    // get all users who have purchased in the past week
    $referrers_obj = [];
    $purchased_this_week = new WP_User_Query( $purchased_args );
    if ( ! empty( $purchased_this_week->get_results() ) ) { // New users have registered
    	foreach ( $purchased_this_week->get_results() as $user ) {
        // If the user was referred by someone, add to Obj
        if ( $referred_by = get_field( 'field_5c65ab52f6ab4', 'user_' . $user->ID ) ) {
          $referrers_obj[$referred_by][] = [
            'id' => $user->ID,
            'purchased' => get_field( 'field_5c65c66748656', 'user_' . $user->ID )
          ];
        }
    	}
    }

    // get all users who were created this week. Filter by !user_has_purchased
    $survey_args = [
      'date_query' => [
        [
          'after'  => $after_date,
          'inclusive' => true
        ]
      ],
      'meta_query' => [
        [
          'key'   => 'user_has_purchased',
          'compare' => 'NOT EXISTS',
        ]
      ]
    ];

    $all_this_week = new WP_User_Query( $survey_args );
    if ( ! empty( $all_this_week->get_results() ) ) { // New users have registered
    	foreach ( $all_this_week->get_results() as $user ) {
        // If the user was referred by someone, add to Obj
        // field_5c65ab52f6ab4 = referrer
        // field_5c65c66748656 = user has purchased
        if ( $referred_by = get_field( 'field_5c65ab52f6ab4', 'user_' . $user->ID ) ) {
          $referrers_obj[$referred_by][] = [
            'id' => $user->ID,
            'purchased' => false
          ];
        }
    	}
    }

    $users = get_users( [
      'role' => 'subscriber',
      'fields' => ['id']
    ] );
    $csv_header = '';
    // Row 1
    $csv_header .= 'date added, ';
    // Row 2
    $csv_header .= 'ref first name, ';
    // Row 3
    $csv_header .= 'ref last name, ';
    // Row 4
    $csv_header .= 'ref email, ';
    // Row 5
    $csv_header .= 'friends who filled out the survey (total), ';
    // Row 6
    $csv_header .= '# friends who filled out the survey (total), ';
    // Row 7
    $csv_header .= 'friends who filled out the survey (' . $after_date . '), ';
    // Row 8
    $csv_header .= '# friends who filled out survey (' . $after_date . '), ';
    // Row 9
    $csv_header .= '# filled out survey text (survey), ';
    // Row 10
    $csv_header .= 'friends who have purchased (total), ';
    // Row 11
    $csv_header .= '# friends who have purchased (total), ';
    // Row 12
    $csv_header .= 'friends who have purchased (' . $after_date . '), ';
    // Row 13
    $csv_header .= '# friends who have purchased (' . $after_date . '), ';
    // Row 14
    $csv_header .= '# purchased this week text, ';
    // Row 15
    $csv_header .= 'total credit, ';
    // Row 16
    $csv_header .= 'credit redeemed, ';
    // Row 17
    $csv_header .= 'active credit amount, ';
    // Row 18
    $csv_header .= 'Collections URL, ';
    // Row 19
    $csv_header .= 'Collections link, ';
    // Row 20
    $csv_header .= 'refer a friend URL,';
    // Row 21
    $csv_header .= 'refer a friend link,';
    // Row 22
    $csv_header .= 'mismatched referrer (' . $after_date . ')';


    //if( count($referrers_obj) > 0 ) {
    $csv_output = '';
    $csv_output .= $csv_header;
    $csv_output .= "\n";
    //foreach( $referrers_obj as $refID=>$refs) {
    foreach( $users as $user ) {
      $user_id = $user->id;
      $user_data = get_userdata( $user_id );
      // Update user purchase date
      // NOTE: REMOVE
      // if( get_field('field_5c65c66748656' , 'user_' . $user_id) ) {
      //   update_field('field_5c954b61605aa', $user_data->user_registered, 'user_' . $user_id);
      // }

      $user_first_name = get_user_meta( $user_id, 'first_name', true );
      $user_first_name .= substr($user_first_name, -1) === 's' ? '\'' : '\'s';
      // field_5c65cddfa2852 = users routine
      // field_5c897d06f3b20 = user type
      // field_5c9541f0a2522 = referral link
      // field_5c6842a992d0f = Collection URL from Hubspot

      // If users have referrals, append #recommended-products to collection URL
      $users_coll_url = get_field('field_5c6842a992d0f', 'user_' . $user_id);
      $users_routine_url = get_field('field_5c65abc5f6ab5', 'user_' . $user_id) ? $users_coll_url.'/#recommended-products' : $users_coll_url;
      $users_routine_link = '<a href="'.$users_routine_url.'">your personal store</a>';
      // If users have referrals, append #referrals to users post link
      $users_routine_id = get_field('field_5c65cddfa2852', 'user_' . $user_id);
      $users_post_link = get_post_field( 'post_name', $users_routine_id );
      $users_post_link = get_home_url() . '/' . $users_post_link;
      $users_referral_url = get_field('field_5c897d06f3b20', 'user_' . $user_id) == 'current' ? $users_post_link . '/#referrals' : $users_post_link;

      $users_referral_link = '<a href="'.$users_referral_url.'">'.$user_first_name.' Haldi friends</a>';
      //$users_referral_link = '=HYPERLINK("'.$users_referral_url.'","'.$user_first_name.' Haldi friends")';
      $active_refs_this_week = [];
      $inactive_refs_this_week = [];

      if( array_key_exists( $user_id, $referrers_obj) ) {
        foreach($referrers_obj[$user_id] as $ref) {
          $ref_data = get_userdata($ref['id']);
          if ( $ref['purchased'] == true ) {
            array_push( $active_refs_this_week, $ref_data->display_name );
          } else {
            array_push( $inactive_refs_this_week, $ref_data->display_name );
          }
        }
      }

      // Calculate Total Haldi Credit and create inactive/active array
      $total_credit = 0;
      $redeemed_credit = 0;
      if( get_field('field_5c90039e56de4', 'user_' . $user_id) != '#N/A' && get_field('field_5c90039e56de4', 'user_' . $user_id) != '' && get_field('field_5c90039e56de4', 'user_' . $user_id) != null) {
        $redeemed_credit = get_field('field_5c90039e56de4', 'user_' . $user_id);
      }

      $active_refs = [];
      $inactive_refs = [];

      if( $user_refs = get_field('field_5c65abc5f6ab5', 'user_' . $user_id) ) {
        foreach( $user_refs as $ref ) {
          if( $ref_data = get_userdata($ref) ) {
            // field_5c65c66748656 = User has purchased
            if ( get_field( 'field_5c65c66748656', 'user_' . $ref ) ) {
              array_push( $active_refs, $ref_data->display_name);
            } else {
              array_push( $inactive_refs, $ref_data->display_name);
            }
          }
        }
      }

      if( count($active_refs) < 10 ) {
        $total_credit = count($active_refs) * 20;
      } else {
        $total_credit = 500 + ((count($active_refs) - 10) * 20);
      }
      $active_credit = $total_credit - $redeemed_credit;

      // User registration date (Row 1)
      $csv_output .= $user_data->user_registered . ',';
      // User first name (Row 2)
      $csv_output .= get_user_meta( $user_id, 'first_name', true ) . ',';
      // User last name (Row 3)
      $csv_output .= get_user_meta( $user_id, 'last_name', true ) . ',';
      // User Email (Row 4)
      $csv_output .= $user_data->user_email . ',';

      // friends who have filled out the survey (total) (Row 5)
      $csv_output .= implode( ' | ', $inactive_refs) . ',';
      // Count friends who fill out the survey (total) (Row 6)
      $csv_output .= count($inactive_refs) . ',';
      // friends who filled out the survey (this week) (Row 7)
      $csv_output .= implode( ' | ', $inactive_refs_this_week) . ',';
      // COUNT friends who filled out the survey (this week) (Row 8)
      $csv_output .= count($inactive_refs_this_week) . ',';
      // TEXT friends who filled out the survey (this week) (Row 9)
      if(count($inactive_refs_this_week) > 0) {
        $csv_output .= count($inactive_refs_this_week) > 1 ? count($inactive_refs_this_week) . ' of your friends,' : 'one of your friends,';
      } else {
        $csv_output .= ' ,';
      }


      // friends who have purchased (total) (Row 10)
      $csv_output .= implode( ' | ', $active_refs) . ',';
      // Count friends who have purchased (total) (Row 11)
      $csv_output .= count($active_refs) . ',';
      // friends who have purchased (this week) (Row 12)
      $csv_output .= implode( ' | ', $active_refs_this_week) . ',';
      // COUNT friends who have purchased (this week) (Row 13)
      $csv_output .= count($active_refs_this_week) . ',';
      // TEXT friends who have purchased (this week) (Row 14)
      if(count($active_refs_this_week) > 0) {
        $csv_output .= count($active_refs_this_week) > 1 ? count($active_refs_this_week) . ' of your friends,' : 'one of your friends,';
      } else {
        $csv_output .= ' ,';
      }

      // Total lifetime Credit (Row 15)
      $csv_output .= '$' . $total_credit . ',';
      // Total Credit Redeemed (Row 16)
      $csv_output .= '$' . $redeemed_credit . ',';
      // Total Credit - Redeemed (Row 17)
      $csv_output .= '$' . $active_credit . ',';

      // User routine URL (Row 18)
      $csv_output .= $users_routine_url . ',';
      // User routine link (Row 19)
      $csv_output .= $users_routine_link . ',';
      // Refer a friend URL (Row 20)
      $csv_output .= $users_referral_url . ',';
      // Refer a friend link (Row 21)
      $csv_output .= $users_referral_link . ',';
      // Mismatched Referrer (Row 22)
      $csv_output .= get_field('field_5ced5644557ac', 'user_' . $user_id);

      // END ROW
      $csv_output .= "\n";
    }
    //}
    return $csv_output;
  }

}

// Instantiate a singleton of this plugin
$csvExport = new CSVExport();
