<?php
set_time_limit ( 0 );
ini_set('memory_limit', '1024M');
function on_create_user_profile_action( $user_id ) {
  //create_haldi_user_profile($user_id);
}

add_action( 'user_register', 'on_create_user_profile_action', 10, 1 );

function on_haldi_profile_update( $user_id, $old_data ) {
  // Users post
  if( $usersPost = get_field('field_5c65cddfa2852', 'user_' . $user_id) ) {
    $args = array(
      'ID' => $usersPost,
      'post_author' => $user_id,
    );
    wp_update_post( $args );
  }
}

add_action( 'profile_update', 'on_haldi_profile_update', 10, 2 );

function create_haldi_user_profile ( $user_id ) {
  $user_meta = get_userdata($user_id);
  $user_roles = $user_meta->roles;

  if ( $user_roles[0] == 'subscriber' ) {

    // If the 'create routine' box is selected, or create profile is set to true AND there is no existing routine, create a post for that user.
    if( get_field('field_5c65cee4fa3dd', 'user_' . $user_id) && !get_field('field_5c65cddfa2852', 'user_' . $user_id) ) {
      // Create post title based on user meta data first and last name
      $profile_title = $user_meta->first_name;
      if(isset($user_meta->last_name)) {
        $profile_title .= ' ' . $user_meta->last_name;
        $profile_title .= substr($user_meta->last_name, -1) === 's' ? '\'' : '\'s';
      } else {
        // No last name field
        $profile_title .= substr($user_meta->first_name, -1) === 's' ? '\'' : '\'s';
      }
      // This is a new user who will get a routine
      $profile_title .= ' Personal Haldi Routine';
      $master = get_post(2877);
      // Set user type as current
      update_field('field_5c897d06f3b20', 'current', 'user_' . $user_id);

      if($userProfile = duplicate_post_create_duplicate( $master, 'draft' )) {
        // Set the users routine as the newly created post
        $updateProfileArgs = [
          'ID' => $userProfile,
          'post_author' => $user_id,
          'post_title' => $profile_title,
          'post_name' => random_post_url(5),
          'post_type' => 'post',
          'post_status' => 'draft',
          'meta_input' => [
            'collection_url' => get_field('field_5c6842a992d0f', 'user_' . $user_id )
          ]
        ];
        wp_update_post($updateProfileArgs);
        update_field('field_5c65cddfa2852', $userProfile, 'user_' . $user_id);
      }

    } else if ($routine = get_field('field_5c65cddfa2852', 'user_' . $user_id) ) {
      // Set the users manually selected routines author as this user.
      $args = array(
        'ID' => $routine,
        'post_author' => $user_id,
      );
      wp_update_post( $args );
    }

    // Update referrals
    update_user_referrals($user_id);
    return true;
  }
}

function random_post_url($length = 5) {
  $unique = uniqid('', true);
  $unique = substr($unique, strlen($unique) - $length, strlen($unique));
  return $unique;
}

function update_haldi_user( $user_id, $old_user_data ) {
  update_user_referrals($user_id);
  if( $collectionsURL = get_field('field_5c6842a992d0f', 'user_' . $user_id ) ) {
    if ($routine = get_field('field_5c65cddfa2852', 'user_' . $user_id) ) {
      update_field('field_5c6842a992d0f', $collectionsURL, $routine);
    }
  }
}

add_action( 'profile_update', 'update_haldi_user', 10, 2 );

function update_user_referrals ($userID) {
  // Update the user who referred this new user to reflect this referral
  if($referredBy = get_field('field_5c65ab52f6ab4', 'user_' . $userID)) {
    // For the person who referred them: Add new user to list of referrals
    $referredByReferrals = get_field('field_5c65abc5f6ab5', 'user_' . $referredBy)
      ? get_field('field_5c65abc5f6ab5', 'user_' . $referredBy)
      : [];
    $user_meta = get_userdata($referredBy);
    if(!in_array($userID, $referredByReferrals)) {
      array_push($referredByReferrals, $userID);
      update_field('field_5c65abc5f6ab5', $referredByReferrals, 'user_' . $referredBy);
    }
  }

  // Update any users who were referred by this new person.
  if($referrals = get_field('field_5c65abc5f6ab5', 'user_' . $userID)) {
    foreach($referrals as $referral) {
      // If the user already has a referrer, don't update this as its a duplicate and two people can't refer the same person.
      if(!get_field('field_5c65ab52f6ab4', 'user_' . $referral)) {
        update_field('field_5c65ab52f6ab4', $userID, 'user_' . $referral);
      }
    }
  }

  return true;
}

// Prevent admin emails from sending to new users
if ( !function_exists( 'wp_new_user_notification' ) ) :
function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
  $user_meta = get_userdata($user_id);
  $user_roles = $user_meta->roles;

  if ( $user_roles[0] == 'subscriber' ) {
    return;
  }
}
endif;

/*------------------------------*\
  Import from CSV
\*------------------------------*/

function import_haldi_users_widget() {
	wp_add_dashboard_widget(
   'import_haldi_users_widget', // Widget slug.
   'Import Haldi Users', // Title.
   'import_haldi_users_widget_function' // Display function.
  );
}

add_action( 'wp_dashboard_setup', 'import_haldi_users_widget' );

function import_haldi_users_widget_function() {
  if (isset($_POST['submit'])) {
    // Add permalink as meta data to all posts before pulling data;
    $args = [
      'post_type' => 'post',
      'posts_per_page' => -1
    ];
    $all_posts_query = new WP_Query( $args );
    if( $all_posts_query->have_posts() ): // If post with email exists
      while( $all_posts_query->have_posts() ) : $all_posts_query->the_post();
        update_post_meta( get_the_ID(), 'permalink', 'http://profile.haldiskin.com/' . basename( get_the_permalink() ) . '/' );
      endwhile;
    endif;
    wp_reset_query();

    $csv_file = $_FILES['csv_file'];
    $csv = array_map('str_getcsv', file($csv_file['tmp_name']));
    $createdUsers = [];
    $updatedUsers = [];
    $failedUsers = [];
    $keys = array_shift($csv);
    foreach ($csv as $i=>$row) {
      $csv[$i] = array_combine($keys, $row);
    }
    // Create user profiles
    foreach ($csv as $key => $row) {
      // if( $key > 100 ) {
      //   continue;
      // }
      $registerUser = register_haldi_user($row);
      if($registerUser == 'added') {
        array_push($createdUsers, $row);
      } else if ($registerUser == 'update') {
        array_push($updatedUsers, $row);
      } else if($registerUser == 'failed') {
        array_push($failedUsers, $row);
      }
    }
    // Set referral info now that all users are present
    foreach ($csv as $key => $row) {
      if( $key > 1000 ) {
        continue;
      }
      create_referral_relationships($row);
    }
    echo count($createdUsers) . ' users imported</br>';
    echo count($updatedUsers) . ' users updated</br>';
    foreach( $failedUsers as $failed ) {
      echo $failed['Email'] . ' could not be edited</br>';
    }
  } else {
    echo '<form action="" method="post" enctype="multipart/form-data">';
    echo '<input type="file" name="csv_file">';
    echo '<input type="submit" name="submit" value="submit">';
    echo '</form>';
  }
}

function register_haldi_user($user) {
  $email = $user['Email'];
  $userDataTemp = get_user_by( 'email', $email );
  if( $userData = get_user_by( 'email', $email ) ) {
    $userID = $userData->ID;
    //User does exist. Update if purchased.
    if ( $user['Became a Customer Date'] != '' ) {
      if (get_field('field_5c65c66748656', 'user_' . $userID) == false) {
        update_field('field_5c65c66748656', true, 'user_' . $userID);
      }
      update_field('field_5c954b61605aa', date('Ymd', strtotime($user['Became a Customer Date'])), 'user_' . $userID);
    }

    // if ( $user['Lifecycle Stage'] == 'Customer' ) {
    //   if (get_field('field_5c65c66748656', 'user_' . $userID) == false) {
    //     update_field('field_5c65c66748656', true, 'user_' . $userID);
    //     update_field('field_5c954b61605aa', date('Ymd'), 'user_' . $userID);
    //   }
    // }

    if ( $userData->user_registered == '0000-00-00 00:00:00' ) {
      wp_update_user( [ 'ID' => $userID, 'user_registered' => date('Y-m-d H:i:s', strtotime($user['Create Date'])) ] );
    }

    if ( $user['Orders count'] > 0 ) {
      // Update order count
      update_field('field_5c68421492d0c', $user['Orders count'], 'user_' . $userID);
    }
    if ( $user['Collections URL'] != '' ) {
      // Update collections URL???????? : // NOTE:
      update_field('field_5c6842a992d0f', $user['Collections URL'], 'user_' . $userID);
    }
    // Update credit redeemed
    if ( $user['credit redeemed'] != '' ) {
      update_field('field_5c90039e56de4', $user['credit redeemed'], 'user_' . $userID);
    }
    // Update referral tracker URL
    if ( $user['Refer a friend tracker URL'] != '' ) {
      update_field('field_5c9541f0a2522', $user['Refer a friend tracker URL'], 'user_' . $userID);
    }
    if( $user['Collections URL'] != '' && stripos($user['Collections URL'], 'profile.haldiskin') != false) {
      update_field('field_5c897d06f3b20', 'current', 'user_' . $userID);
    }
    return 'update';
  } else {
    // User does not exist. Create user.
    $password = wp_generate_password( 12, true );
    $first = $user['First Name'];
    $last = $user['Last Name'];
    // $iso_date = date( 'Y-m-d H:i:s');
    // $gmt_date = get_date_from_gmt( $iso_date, 'U' );
    // $user_registered = gmdate( 'Y-m-d H:i:s', $gmt_date );
    $userArgs = [
      'user_pass' => $password,
      'user_email' => $email,
      'user_login' => $email,
      'first_name' => $first,
      'last_name' => $last,
      'user_registered' => $user['Create Date'],
      'role' => 'subscriber'
    ];
    if( $userID = wp_insert_user($userArgs) ) {
      if ( is_object($userID) ) {
        return 'failed';
      }
      if( $user['City'] != '' ) {
        update_field('field_5c6840c892d09', $user['City'], 'user_' . $userID);
      }
      if ( $user['State/Region'] != '' ) {
        update_field('field_5c6841e092d0b', $user['State/Region'], 'user_' . $userID);
      }
      if( $user['Shopify Created At'] != '' ) {
        $shopifydate = update_field('field_5c6840d392d0a', $user['Shopify Created At'], 'user_' . $userID);
      }
      if ( $user['Orders count'] > 0 ) {
        update_field('field_5c68421492d0c', $user['Orders count'], 'user_' . $userID);
      }
      // Add has purchased
      if ( $user['Became a Customer Date'] != '' ) {
        if (get_field('field_5c65c66748656', 'user_' . $userID) == false) {
          update_field('field_5c65c66748656', true, 'user_' . $userID);
          update_field('field_5c954b61605aa', date('Ymd', strtotime($user['Became a Customer Date'])), 'user_' . $userID);
        }
      }

      // if ( $user['Lifecycle Stage'] == 'Customer' ) {
      //   if (get_field('field_5c65c66748656', 'user_' . $userID) == false) {
      //     update_field('field_5c65c66748656', true, 'user_' . $userID);
      //     update_field('field_5c954b61605aa', date('Ymd'), 'user_' . $userID);
      //   }
      // }

      // Add Collections URL
      if ( $user['Collections URL'] != '' ) {
        update_field('field_5c6842a992d0f', $user['Collections URL'], 'user_' . $userID);
      }
      // Add credits redeemed
      if ( $user['credit redeemed'] != '' ) {
        update_field('field_5c90039e56de4', $user['credit redeemed'], 'user_' . $userID);
      }
      // Add referral tracker URL
      if ( $user['Refer a friend tracker URL'] != '' ) {
        update_field('field_5c9541f0a2522', $user['Refer a friend tracker URL'], 'user_' . $userID);
      }

      if( $user['Collections URL'] != '' ) {
        $args = array(
          'name'        => basename($user['Collections URL']),
          'post_type'   => 'post',
          'post_status' => ['publish', 'draft'],
          'numberposts' => 1
        );
        $user_post = get_posts($args);
        if( $user_post ) { // If post with email exists
          // Update users info
          update_field('field_5c65cddfa2852', $user_post[0]->ID, 'user_' . $userID);
          // If user has collection URL, update it on the routine
          if( $collectionsURL = get_field('field_5c6842a992d0f', 'user_' . $userID ) ) {
            update_field('field_5c6842a992d0f', $collectionsURL, $user_post[0]->ID);
          }
          $args = array(
            'ID' => $user_post[0]->ID,
            'post_author' => $userID,
          );
          wp_update_post( $args );
          if( update_user_referrals($userID) ) {
            return 'added';
          }
        } else { // No user exists with this email
          // Fire on register script
          if( create_haldi_user_profile( $userID, $user['Collections URL'] ) ) {
            // $user = new WP_User( $userID );
            // echo $user->first_name;
            return 'added';
          }
        }

      } else {
        create_haldi_user_profile( $userID );
      }

      wp_reset_query();

    } else {
      return 'failed';
    }
  }

  return;
}

function create_referral_relationships ($user) {
  $userData = get_user_by( 'email', $user['Email'] );
  if ( isset($userData->ID) ) {
    $userID = $userData->ID;

    if( $recommended = $user['Who recommended'] ) {
      $first = '';
      $last = '';
      $nameArray = explode(" ", $recommended, 2);
      if( isset($nameArray[0]) ) {
        $first = esc_sql($nameArray[0]);
      }
      if( isset($nameArray[1]) ) {
        $last = esc_sql($nameArray[1]);
      }

      // Search for user by first and last name :(
      global $wpdb;
      $users = $wpdb->get_results( "SELECT user_id
        FROM $wpdb->usermeta
        GROUP BY user_id
        HAVING sum(meta_key = 'first_name'
          AND meta_value = '$first') > 0
          AND sum(meta_key = 'last_name'
            AND meta_value = '$last') > 0 ");
            //$the_users = $wpdb->get_results("SELECT DISTINCT $wpdb->users.* FROM $wpdb->users INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE (um1.meta_key = 'uRPhICRS_capabilities' AND um1.meta_value LIKE '%agent%') AND ((um2.meta_key = 'first_name' OR um2.meta_key = 'last_name') AND um2.meta_value LIKE '%".$search_term."%');");
            if( $users ) {
              $referredBy = $users[0]->user_id;
              update_field('field_5c65ab52f6ab4', $referredBy, 'user_' . $userID);
              delete_field('field_5ced51c6d4f56', 'user_' . $userID);
              update_user_referrals($userID);
            } else { // Can't find the user. Add to failed referrer field
              update_field('field_5ced5644557ac', $recommended, 'user_' . $userID);
            }
          }
  }
}

/*------------------------------*\
  Display referral functions
\*------------------------------*/

function displayReferrals($refs) {
  foreach($refs as $ref) {
    $refData = get_userdata($ref);
    echo displayRef($refData);
  }
}

function displayRef($refData) { ?>
  <div data-referral-id="<?php echo $refData->ID; ?>" data-referral-email="<?php echo $refData->user_email; ?>" class="profile-preview">
    <div class="preview-initials">
      <?php echo substr($refData->first_name, 0, 1) . substr($refData->last_name, 0, 1); ?>
    </div>
    <span><?php echo $refData->first_name; ?></span>
    <div class="preview-popup">
      <span><?php echo $refData->first_name . ' ' . $refData->last_name; ?></span>
    </div>
  </div>
<?php
}

function find_routines_where( $where ) {
  $where = str_replace("meta_key = 'recommended_products_$", "meta_key LIKE 'recommended_products_%", $where);
  return $where;
}

add_filter('posts_where', 'find_routines_where');

function add_user_edit_link_to_post_edit(){
  $screen = get_current_screen();
  if($screen->post_type == 'post') {
    if(isset($_REQUEST['post'])) {
      $postID = $_REQUEST['post'];
      $userID = get_post_field( 'post_author', $postID );
      $userEditLink = get_edit_user_link($userID);
       ?>
       <script>
       jQuery(document).ready(function(){
         setTimeout(function() {
           jQuery("body.post-type-post .edit-post-header-toolbar").append('<a href="<?php echo $userEditLink; ?>" class="page-title-action">Edit User</a>');
         }, 0);
       });
       </script>
    <?php
    }
  }
}

add_action('admin_head','add_user_edit_link_to_post_edit');

?>
