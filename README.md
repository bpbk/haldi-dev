# Haldi Referrals

## Install WP

* Create directory with WP install files from https://wordpress.org/download/
* Change wp-config-sample.php to wp-config.php
* Edit wp-config.php to reflect local database (I use MAMP)
* cd into themes directory: `cd wp-content/themes/`
* Clone the repo
* cd into theme: `cd haldi`
* Run `npm install`
* Run `npm run watch`
* Follow steps to setup WP ( any credentials will work )
* Add wp sync plugin to plugins directory: wp-content/plugins
* Login to WP and activate wp sync plugin in plugins area
* Go to Tools -> Migrate DB and select 'Pull' and add connection settings

## Pull to AWS

* after making changes run `npm run build`
* push changes to git
* Save pem key somewhere
* ssh into EC2 instance with `ssh -i {directory of pem}/{pemfile}.pem ubuntu@ec2-3-82-123-197.compute-1.amazonaws.com`
* cd into theme `cd apps/wordpress/htdocs/wp-content/themes/haldi`
* pull from git
