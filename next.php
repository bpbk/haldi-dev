<div class="whats-next">
  <h2>So what's next?</h2>
  <div class="grid slider">
    <div class="statement">
      <span class=icon-wrapper>
        <img class="icon" src="<?php bloginfo('template_url'); ?>/img/icons/shop.png"/>
      </span>
      <h6>Shop your routine</h6>
      <p>No retail markup on our end and shipping is always free.</p>
    </div>
    <div class="statement">
      <span class=icon-wrapper>
        <img class="icon" src="<?php bloginfo('template_url'); ?>/img/icons/chat.png"/>
      </span>
      <h6>Reach out anytime</h6>
      <p>Seriously, we geek out over skincare questions.</p>
    </div>
    <div class="statement">
      <span class=icon-wrapper>
        <img class="icon" src="<?php bloginfo('template_url'); ?>/img/icons/happy.png"/>
      </span>
      <h6>100% guarantee</h6>
      <p>If you don't love a product, we'd be happy to take it back.</p>
    </div>
  </div>
</div>
